//
//  CryptoCurrencyTests.swift
//  CryptoCurrencyTests
//
//  Created by Maksim Avksentev on 11/1/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import XCTest
@testable import CryptoCurrency

class CryptoCurrencyTests: XCTestCase {

    func testFirstAPI() {
        
        self.execute(service: FirstService())
    }
    
    func testSecondAPI() {
        
        self.execute(service: SecondService())
    }
    
    func execute(service: Service) {

        let expect = expectation(description: service.baseURL)
        
        Networking.requestJSON(apiPath: service.baseURL + service.path, apiParameters: service.params, success: { (data) in
            
            guard let data = try? service.jsonData(from: data), let coinContract = service.getCoin(from: data) else {
                
                XCTFail(ErrorTypes.ObjectParser.localizedDescription)
                return
            }
            
            print(coinContract.get())
            expect.fulfill()
        }) { (error) in
            
            XCTFail(error.localizedDescription)
            expect.fulfill()
        }
        
        waitForExpectations(timeout: 5.0, handler: nil)
    }
}
