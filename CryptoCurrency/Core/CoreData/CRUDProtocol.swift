//
//  CRUDProtocol.swift
//  CryptoCurrency
//
//  Created by Maksim Avksentev on 11/4/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import Foundation
import CoreData

protocol CRUDProtocol {
    
    func create<T: NSManagedObject>(_ context: NSManagedObjectContext, parametrs: [String: Any?]) -> T?
    
    func read<T: NSManagedObject>(_ context: NSManagedObjectContext, parametrs: [String: Any?]?, sortByKey: String?, ascending: Bool) -> [T]
    
    func createOrUpdate<T: NSManagedObject>(_ context: NSManagedObjectContext, primary: [String: Any?], secondary: [String: Any?]) -> T?
    
    func delete<T: NSManagedObject>(_ context: NSManagedObjectContext, parametrs: [String: Any?]) -> T?
}
