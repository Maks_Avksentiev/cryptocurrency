//
//  TicketDb.swift
//  CryptoCurrency
//
//  Created by Maksim Avksentev on 11/5/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import Foundation
import CoreData

extension TicketDb {
    
    class func get(from service: Service) -> [TicketDb] {
        
        return CCCoreData.manager.read(CCCoreData.manager.currentThreadContext(), parametrs: ["source": service.baseURL], sortByKey: "lastUpdate", ascending: true) 
    }
    
    class func create(ticket: Ticket) -> Ticket? {
 
        let data: [TicketDb] = CCCoreData.manager.read(CCCoreData.manager.currentThreadContext(), parametrs: ["lastUpdate": ticket.lastUpdate, "source": ticket.source])
        
        if data.count == 0 {
            
            let ticketDb: TicketDb? = CCCoreData.manager.create(CCCoreData.manager.currentThreadContext(), parametrs: ["id": ticket.id, "lastUpdate": ticket.lastUpdate, "price": ticket.price, "percentChange": ticket.percentChange, "source": ticket.source])
            
            return ticketDb != nil ? ticket : nil
        }
            
        return nil
        
    }
}
