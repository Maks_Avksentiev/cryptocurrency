//
//  CCCoreData.swift
//  CryptoCurrency
//
//  Created by Maksim Avksentev on 11/4/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import CoreData
import RxSwift
import RxCocoa

class CCCoreData: NSObject {

    private static let sharedInstance = CCCoreData()
    
    static var manager: CCCoreData {
        return sharedInstance
    }
    
    private let persistentContainer = NSPersistentContainer(name: "CryptoCurrency")
    
    private let dispose = DisposeBag()
    
    var viewContext: NSManagedObjectContext {
        
        return self.persistentContainer.viewContext
    }
    
    var backgroundContext: NSManagedObjectContext {
        
        return self.persistentContainer.newBackgroundContext()
    }
    
    //MARK: - Init
    private override init() {
        
        super.init()
        
        self.initialize()
    }
    
    deinit {
        
        NotificationCenter.default.removeObserver(self)
    }
    
    //MARK: - Main
    func currentThreadContext() -> NSManagedObjectContext {
        
        return Thread.isMainThread ? self.viewContext : self.backgroundContext
    }
    
    func performBackgroundTask(_ block: @escaping (NSManagedObjectContext) -> Void) {
        
        self.persistentContainer.performBackgroundTask(block)
    }
    
    func save(_ context: NSManagedObjectContext) {
        
        if context.hasChanges {
            
            do{
                try context.save()
            } catch {
                
                //FIXME: log error
                //                let nserror = error as NSError
                //                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

//MARK: - Private
extension CCCoreData {
    
    private func initialize(completionHandler: @escaping (Bool) -> Void = {_ in}) {
        
        self.persistentContainer.loadPersistentStores { (_, error) in
            
            guard error == nil else {
                
                //FIXME: log error
                completionHandler(false)
                return
            }
            
            completionHandler(true)
        }
        
        self.registerForDidSave()
    }
    
    fileprivate func registerForDidSave() {
     
        NotificationCenter.default.rx.notification(.NSManagedObjectContextDidSave)
            .subscribe(onNext: { (notification) in
                
                if let context = notification.object as? NSManagedObjectContext, context.persistentStoreCoordinator != self.viewContext.persistentStoreCoordinator || context == self.viewContext {
                    
                    return
                }
                
                Queues.main.async {
                    self.viewContext.mergeChanges(fromContextDidSave: notification)
                }
            }).disposed(by: self.dispose)
        
        Queues.main.async {
            let _ = self.viewContext
            Queues.requestsBackground.async {
                let _ = self.backgroundContext
            }
        }
    }
}
