//
//  Services.swift
//  CryptoCurrency
//
//  Created by Maksim Avksentev on 11/5/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import Foundation
import UIKit

enum Services: Int {
    
    case first = 0
    case second = 1
    
    var endpoint: Service {
        
        switch self {
        case .first:
            return FirstService()
        case .second:
            return SecondService()
        }
    }
    
    func getEndpoint() -> Service {
        
        switch self {
        case .first:
            return Services.all[0]
        case .second:
            return Services.all[1]
        }
    }
    
    func color() -> UIColor {
        
        switch self {
        case .first:
            return UIColor(hexString: "9F840A")//F1C80B
        case .second:
            return UIColor(hexString: "3A6B0D")//59AC0F
        }
    }
    
    func source() -> String {
        
        switch self {
        case .first:
            return "https://api.coinmarketcap.com/"
        case .second:
            return "https://min-api.cryptocompare.com/"
        }
    }
    
    func set(lineVisable: Bool) {
    
        UserDefaults.standard.set(lineVisable, forKey: self.source())
    }
    
    func getLineVisable() -> Bool {
        
        guard let value = UserDefaults.standard.value(forKey: self.source()) as? Bool else {
            
            self.set(lineVisable: true)
            
            return true
        }
        
        return value
    }
    
    static let all = [Services.first.endpoint, Services.second.endpoint]
}
