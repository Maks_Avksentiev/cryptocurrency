//
//  SecondService.swift
//  CryptoCurrency
//
//  Created by Maksim Avksentev on 11/5/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import Foundation

struct SecondService: Service {
    
    var baseURL = Services.second.source()
    var path = "data/pricemultifull"
    var params = ["fsyms": "BTC", "tsyms": "USD"]
    
    var source = Services.second
    
    func jsonData(from data: Data) throws -> Data {
        
        guard let json = (((try? JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject])??["RAW"] as? [String: AnyObject])?["BTC"] as? [String: AnyObject])?["USD"], let jsonData = try? JSONSerialization.data(withJSONObject: json, options: []) else {
            
            throw ErrorTypes.ObjectParser
        }
        
        return jsonData
    }
    
    func getCoin(from data: Data) -> TicketContract? {
        
        return try? JSONDecoder().decode(SecondCoin.self, from: data)
    }
}
