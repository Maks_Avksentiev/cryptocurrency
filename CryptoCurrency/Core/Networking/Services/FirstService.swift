//
//  FirstService.swift
//  CryptoCurrency
//
//  Created by Maksim Avksentev on 11/5/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import Foundation

struct FirstService: Service {
    
    var baseURL = Services.first.source()
    var path = "v2/ticker/"
    var params = ["convert": "BTC", "limit": "1"]
    
    var source = Services.first
    
    func jsonData(from data: Data) throws -> Data {
        
        guard let json = (((try? JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject])??["data"] as? [String: AnyObject])?["1"] as? [String: AnyObject]), let jsonData = try? JSONSerialization.data(withJSONObject: json, options: []) else {
            
            throw ErrorTypes.ObjectParser
        }
        
        return jsonData
    }
    
    func getCoin(from data: Data) -> TicketContract? {
        
        return try? JSONDecoder().decode(Coin.self, from: data)
    }
}
