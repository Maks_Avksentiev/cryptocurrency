//
//  Service.swift
//  CryptoCurrency
//
//  Created by Maksim Avksentev on 11/5/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import Foundation

protocol Service {
    
    var baseURL: String {get set}
    var path: String {get set}
    var params: [String: String] {get set}
    
    var source: Services {get set}
    
    func jsonData(from data: Data) throws -> Data
    func getCoin(from data: Data) -> TicketContract?
}
