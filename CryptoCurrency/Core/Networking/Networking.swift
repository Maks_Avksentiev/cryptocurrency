//
//  Networking.swift
//  CryptoCurrency
//
//  Created by Maksim Avksentev on 11/4/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import UIKit
import Alamofire

enum ErrorTypes: Error {
    
    case Network
    case ObjectParser
    case WrongTime
    
    var localizedDescription: String {
        
        switch self {
        case .Network:
            return "Trubles with Networking"
        case .ObjectParser:
            return "Trubles with parsing objects"
        case .WrongTime:
            return "Only 1 request per one minute"
        }
    }
}

class Networking: NSObject {
    
    static var requestDate: Date!
    
    static var reachability = Reachability()
    
    class func update(service: Service, completion: @escaping ((Ticket?, ErrorTypes?) -> Void)) {
        
        self.requestJSON(apiPath: service.baseURL + service.path, apiParameters: service.params, success: { (data) in
            
            guard let data = try? service.jsonData(from: data), let coinContract = service.getCoin(from: data) else {
                
                completion(nil, ErrorTypes.ObjectParser)
                return
            }
            
            completion(coinContract.get(), nil)
        }) { (error) in
            
            completion(nil, error)
        }
    }
    
    class func requestJSON(apiPath: String, apiParameters: [String: String], success: @escaping (Data) -> (), failure: @escaping (ErrorTypes) -> () ) {
        
        Alamofire.request(apiPath, parameters: apiParameters)
            .responseData { response in
                switch response.result {
                case .success:
                    success(response.result.value!)
                default:
                    failure(.Network)
                }
        }
    }
}
