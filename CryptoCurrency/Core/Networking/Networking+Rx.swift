//
//  Networking+Rx.swift
//  CryptoCurrency
//
//  Created by Maksim Avksentev on 11/5/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

extension Reactive where Base: Networking {
    
    static func update(service: Service) -> Single<Ticket> {
    
        return Observable<Ticket>.create { observer -> Disposable in
            
            Networking.update(service: service, completion: { (ticket, error) in
                
                guard let ticket = ticket, error == nil else {
                    
                    observer.onError(error!)
                    return
                }
                
                observer.onNext(ticket)
                observer.onCompleted()
            })
            
            return Disposables.create()
        }.asSingle()
    }
    
    static func updateAll() -> Single<[Ticket]> {
        
        guard (Networking.requestDate != nil && Date() >= Networking.requestDate) || Networking.requestDate == nil else {
            
            return Observable.error(ErrorTypes.WrongTime).asSingle()
        }
        
        return Observable.zip(self.update(service: Services.all[0]).asObservable(), self.update(service: Services.all[1]).asObservable()).map({[$0, $1]}).asSingle()
    }
}
