//
//  Ticket.swift
//  CryptoCurrency
//
//  Created by Maksim Avksentev on 11/5/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import Foundation

protocol TicketContract {
    
    func get() -> Ticket
}

class Ticket {
    
    var id: String!
    var lastUpdate: Double!
    var price: Double!
    var percentChange: Double!
    var source: String!
    
    init(coin: Coin) {
        
        self.id = String(format: "%.2f", Date().timeIntervalSince1970)
        self.lastUpdate = coin.id
        self.price = coin.quotes.USD.price
        self.percentChange = coin.quotes.USD.percentChange
        self.source = Services.first.endpoint.baseURL
    }
    
    init(coin: SecondCoin) {
        
        self.id = String(format: "%.2f", Date().timeIntervalSince1970)
        self.lastUpdate = coin.id
        self.price = coin.price
        self.percentChange = coin.percentChange
        self.source = Services.second.endpoint.baseURL
    }
    
    init(ticket: TicketDb) {
        
        self.id = ticket.id
        self.lastUpdate = ticket.lastUpdate
        self.price = ticket.price
        self.percentChange = ticket.percentChange
        self.source = ticket.source
    }
}
