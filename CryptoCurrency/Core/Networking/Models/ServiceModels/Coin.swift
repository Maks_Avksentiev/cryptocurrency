//
//  Coin.swift
//  CryptoCurrency
//
//  Created by Maksim Avksentev on 11/5/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import Foundation

struct Coin: Decodable {
    
    var id: Double
    var quotes: Quotes
    
    enum CodingKeys: String, CodingKey {
        
        case id = "last_updated"
        case quotes = "quotes"
    }
    
    struct Quotes: Codable {
        
        var USD: Quote
        
        enum CodingKeys: String, CodingKey {
            case USD
        }
    }
    
    struct Quote: Codable {
        
        var price: Double
        var percentChange: Double
        
        enum CodingKeys: String, CodingKey {
            
            case price = "price"
            case percentChange = "percent_change_24h"
        }
    }
    
    init(from decoder: Decoder) throws {
        
        guard let container = try? decoder.container(keyedBy: CodingKeys.self), let id = try? container.decode(Double.self, forKey: .id), let quotes = try? container.nestedContainer(keyedBy: Quotes.CodingKeys.self, forKey: CodingKeys.quotes), let quote = try? quotes.decode(Quote.self, forKey: .USD) else {
            
            throw ErrorTypes.ObjectParser
        }
        
        self.id = id
        self.quotes = Quotes(USD: quote)
    }
}

extension Coin: TicketContract {
    
    func get() -> Ticket {
        
        return Ticket(coin: self)
    }
}
