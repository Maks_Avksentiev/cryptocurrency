//
//  SecondCoin.swift
//  CryptoCurrency
//
//  Created by Maksim Avksentev on 11/5/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import Foundation

struct SecondCoin: Codable {
    
    var id: Double
    var price: Double
    var percentChange: Double
    
    enum CodingKeys: String, CodingKey {
        
        case id = "LASTUPDATE"
        case price = "PRICE"
        case percentChange = "CHANGEPCT24HOUR"
    }
}

extension SecondCoin: TicketContract {
    
    func get() -> Ticket {
        
        return Ticket(coin: self)
    }
}
