//
//  CCMainController.swift
//  CryptoCurrency
//
//  Created by Maksim Avksentev on 11/4/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import UIKit

class CCMainController: CCBaseController {
    
    override class var identifier: String {
        return "CCMainController"
    }
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var chart: ChartView!
    @IBOutlet weak var labelLeadingMarginConstraint: NSLayoutConstraint!
    @IBOutlet weak var valueLabel: UILabel!
    weak var refreshControl: UIRefreshControl!
    
    var labelLeadingMarginInitialConstant: CGFloat!
    
    var presenter: CCMainPresenter!
   
    //MARK: - Init
    required convenience init?(coder aDecoder: NSCoder) {
        
        self.init(nibName: CCMainController.identifier, bundle: Bundle.main)
    }
    
    //MARK: - LifeCycle
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.presenter.checkSelection()
        self.presenter.reload()
        self.chart.setNeedsDisplay()
    }
}

//MARK: - Configure
extension CCMainController {
    
    override func configure() {
        
        super.configure()
        
        self.labelLeadingMarginInitialConstant = self.labelLeadingMarginConstraint.constant
        self.tableViewConfigure()
        self.connect()
    }
    
    fileprivate func connect() {
    
        self.presenter = CCMainPresenter()
        self.presenter.view = self
        self.presenter.initialize()
        self.presenter.provider.presenter = self.presenter
        self.tableView.dataSource = self.presenter.provider
        self.tableView.delegate = self.presenter
        
        self.refreshControl.beginRefreshing()
        self.presenter.provider.initialize()
    }
    
    fileprivate func tableViewConfigure() {
        
        self.tableView.allowsMultipleSelection = true
        self.tableView.register(CCTableCell.nib(), forCellReuseIdentifier: CCTableCell.identifier)
    }
}

//MARK: - CCMainView
extension CCMainController: CCMainView {
    
    func present(_ controller: UIViewController) {
        
        self.present(controller, animated: true, completion: nil)
    }
}
