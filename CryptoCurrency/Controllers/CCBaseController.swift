//
//  CCBaseController.swift
//  CryptoCurrency
//
//  Created by Maksim Avksentev on 11/4/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import UIKit

class CCBaseController: UIViewController {

    class var identifier: String {
        
        return "CCBaseController"
    }
    
    fileprivate var gradientView: GradientView!
    
    //MARK: - LifeCycle
    override func loadView() {
        
        super.loadView()
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        self.configure()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        self.addGradient()
    }
}

//MARK: - Configure
extension CCBaseController {
    
    @objc func configure() {
        
    }
    
    fileprivate func addGradient() {
        
        guard self.gradientView == nil else {
            
            return
        }
        
        self.gradientView = GradientView(frame: self.view.frame)
        self.gradientView.startColor = UIColor(hexString: "6982ED")
        self.gradientView.endColor = UIColor(hexString: "383E58")
        
        self.view.insertSubview(self.gradientView, at: 0)
    }
}

//MARK: - ControllerLoader
extension CCBaseController: ControllerLoader {}
