//
//  ControllerLoader.swift
//  CryptoCurrency
//
//  Created by Maksim Avksentev on 11/4/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import UIKit

protocol ControllerLoader where Self: UIViewController {
    
    static var identifier: String {get}
    
    init()
    
    func load()
    func configure()
}

extension ControllerLoader {
    
    init() {
        
        self.init(nibName: Self.identifier, bundle: Bundle.main)
        self.load()
    }
    
    init?(coder aDecoder: NSCoder) {

        self.init(coder: aDecoder)
    }
    
    func configure() {}
    
    func load() {}
}
