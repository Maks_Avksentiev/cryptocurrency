//
//  Queues.swift
//  CryptoCurrency
//
//  Created by Maksim Avksentev on 11/4/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import Foundation

struct Queues {
    
    static let background = ThreadsManager.shared.getBackgroundThread()
    static let requestsBackground = ThreadsManager.shared.getRequestsBackgroundQueue()
    static let main = DispatchQueue.main
}

fileprivate class ThreadsManager {
    
    static let shared = ThreadsManager()
    
    func getBackgroundThread() -> DispatchQueue {
        
        struct Static {
            
            static var instance = DispatchQueue(label: "requests.backgound.queue", attributes: [])
        }
        
        return Static.instance
    }
    
    func getRequestsBackgroundQueue() -> DispatchQueue {
        
        struct Static {
            
            static var instance = DispatchQueue(label: "requests.backgound.queue", qos: .userInteractive)
        }
        
        return Static.instance
    }
}
