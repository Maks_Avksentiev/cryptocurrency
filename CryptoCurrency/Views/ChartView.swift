//
//  ChartView.swift
//  CryptoCurrency
//
//  Created by Maksim Avksentev on 11/5/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import Foundation
import SwiftChart

class ChartView: Chart {
    
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
        
        self.labelFont = UIFont.systemFont(ofSize: 7)
        self.labelColor = UIColor(white: 0.8, alpha: 1.0)
//        self.hideHighlightLineOnTouchEnd = true
        self.bottomInset = 50
        self.showXLabelsAndGrid = false
    }
}
