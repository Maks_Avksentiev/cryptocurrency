//
//  GradientView.swift
//  CryptoCurrency
//
//  Created by Maksim Avksentev on 11/4/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import UIKit

@IBDesignable class GradientView: UIView {
    
    @IBInspectable var startColor: UIColor = .black {
        didSet {
            self.updateColors()
        }
    }
    @IBInspectable var endColor: UIColor = .white {
        didSet {
            self.updateColors()
        }
    }
    @IBInspectable var startLocation: Double = 0.05 {
        didSet {
            self.updateLocations()
        }
    }
    @IBInspectable var endLocation: Double = 0.95 {
        didSet {
            self.updateLocations()
        }
    }
    @IBInspectable var horizontalMode: Bool = false {
        didSet {
            self.updatePoints()
        }
    }
    @IBInspectable var diagonalMode: Bool = false {
        didSet {
            self.updatePoints()
        }
    }
    
    override class var layerClass: AnyClass {
        
        return CAGradientLayer.self
    }
    
    var gradientLayer: CAGradientLayer {
        return layer as! CAGradientLayer
    }
    
    //MARK: - LifeCycle
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        self.updatePoints()
        self.updateLocations()
        self.updateColors()
    }
    
    //MARK: - MAIN
    func updatePoints() {
        
        if horizontalMode {
            
            self.gradientLayer.startPoint = diagonalMode ? CGPoint(x: 1, y: 0) : CGPoint(x: 0, y: 0.5)
            self.gradientLayer.endPoint = diagonalMode ? CGPoint(x: 0, y: 1) : CGPoint(x: 1, y: 0.5)
        } else {
            
            self.gradientLayer.startPoint = diagonalMode ? CGPoint(x: 0, y: 0) : CGPoint(x: 0.5, y: 0)
            self.gradientLayer.endPoint   = diagonalMode ? CGPoint(x: 1, y: 1) : CGPoint(x: 0.5, y: 1)
        }
    }
    
    func updateLocations() {
        
        self.gradientLayer.locations = [self.startLocation as NSNumber, self.endLocation as NSNumber]
    }
    
    func updateColors() {
       
        self.gradientLayer.colors = [self.startColor.cgColor, self.endColor.cgColor]
    }
    
}
