//
//  CCTableCell.swift
//  CryptoCurrency
//
//  Created by Maksim Avksentev on 11/5/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import UIKit

class CCTableCell: UITableViewCell {

    class var identifier: String {
        return "CCTableCell"
    }
    
    @IBOutlet weak var iconView: UIImageView!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var changeInDay: UILabel!

    //MARK: - LifeCycle
    override func awakeFromNib() {
        
        super.awakeFromNib()
        
        self.iconView.layer.cornerRadius = 9.0
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        
        super.setSelected(selected, animated: animated)
        
        self.contentView.alpha = selected ? 1.0 : 0.5
    }
    
    //MARK: - Main
    func fill(with ticket: Ticket) {
        
        self.price.text = String(format: "%.2f", ticket.price)
        self.changeInDay.text = String(format: "%.2f", ticket.percentChange)
        self.changeInDay.textColor = ticket.percentChange > 0 ? UIColor.green : UIColor.red
    }
}

//MARK: - Nib
extension CCTableCell {
    
    static func nib() -> UINib {
        
        return UINib(nibName: self.identifier, bundle: Bundle.main)
    }
}
