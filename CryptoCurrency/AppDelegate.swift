//
//  AppDelegate.swift
//  CryptoCurrency
//
//  Created by Maksim Avksentev on 11/1/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

@UIApplicationMain
class AppDelegate: UIResponder {

    var window: UIWindow?
    var dispose = DisposeBag()
}

//MARK: - UIApplicationDelegate
extension AppDelegate: UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        return true
    }
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        
        try? Networking.reachability?.startNotifier()
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
        
        Networking.reachability?.stopNotifier()
    }
    
    
}

//MARK: - Private
extension AppDelegate {
    
    private func configureReachability() {
        
        Networking.reachability?.rx.reachabilityChanged
            .subscribe(onNext: { (reachability) in
                Queues.main.async {
                    if reachability.currentReachabilityStatus == .notReachable, !(UIApplication.topViewController() is UIAlertController) {
                        
                        let controller = UIAlertController(title: "Attention", message: "Bad connection", preferredStyle: .alert)
                        controller.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
                        
                        UIApplication.topViewController()?.present(controller, animated: true, completion: nil)
                    }
                }
            })
            .disposed(by: self.dispose)
    }
}

