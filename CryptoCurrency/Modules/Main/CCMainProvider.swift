//
//  CCMainProvider.swift
//  CryptoCurrency
//
//  Created by Maksim Avksentev on 11/5/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

class CCMainProvider: NSObject {

    var tickets = [Ticket]()
    
    weak var presenter: CCMainPresenter!
    
    var dispose = DisposeBag()
    
    //MARK: - Main
    func initialize(completion: ((Bool) -> Void)? = nil) {
        
        self.loadDatabase()
        self.update()
    }
    
    func loadDatabase() {
        
        Services.all.forEach({ service in
            
            TicketDb.get(from: service).forEach({ (ticket) in
                
                self.tickets.append(Ticket(ticket: ticket))
            })
        })
    }
    
    func update() {
        
        Networking.rx.updateAll().subscribe(onSuccess: { (tickets) in
            
            let beforeRefresh = self.tickets.count
            tickets.forEach({ticket in
                
                if let newTicket = TicketDb.create(ticket: ticket) {
                    
                    self.tickets.append(newTicket)
                }
            })
            
            let state = beforeRefresh != self.tickets.count
            if state {
                Networking.requestDate = Date().addingTimeInterval(60)
            }
            
            self.presenter.endRefresh(withUpdates: state)
        }, onError: { (error) in
            
            self.presenter.endRefresh()
            Queues.main.asyncAfter(deadline: .now() + 0.5, execute: {
                self.presenter.present(error: error as! ErrorTypes)
            })
            
        }).disposed(by: self.dispose)
        
        return
    }

    //MARK: - Tickets
    func getTickets(for service: Service) -> [Ticket] {
        
        return self.tickets.filter({$0.source == service.baseURL})
    }
    
    func getLatest(for service: Service) -> Ticket? {
        
        return self.getTickets(for: service).last
    }
}

extension CCMainProvider: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return Services.all.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCell(withIdentifier: CCTableCell.identifier, for: indexPath) as? CCTableCell, let service = Services(rawValue: indexPath.row) else {
            
            return UITableViewCell()
        }
        
        cell.iconView.backgroundColor = service.color()
        
        if let ticket = self.getLatest(for: service.getEndpoint()) {
            
            cell.fill(with: ticket)
        }
        
        return cell
    }
}
