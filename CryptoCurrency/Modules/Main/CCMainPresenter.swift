//
//  CCMainPresenter.swift
//  CryptoCurrency
//
//  Created by Maksim Avksentev on 11/5/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import UIKit
import SwiftChart

class CCMainPresenter: NSObject {
    
    var provider = CCMainProvider()
    weak var view: CCMainView!
    
    //MARK: - Init
    func initialize() {
        
        self.configureRefresh()
        self.view.chart.delegate = self
        self.view.valueLabel.numberOfLines = 0
        self.resetLabel()
    }
    
    //MARK: - Refresh
    fileprivate func configureRefresh() {
        
        self.view.tableView.refreshControl = UIRefreshControl()
        self.view.refreshControl = self.view.tableView.refreshControl
        self.view.refreshControl.tintColor = UIColor(white: 0.8, alpha: 1.0)
        self.view.tableView.refreshControl?.addTarget(self, action: #selector(refreshData(_:)), for: .valueChanged)
    }
    
    func startRefresh() {
        
        guard self.view.refreshControl != nil, !self.view.refreshControl.isRefreshing else {
            
            return
        }
        
        self.view.refreshControl.beginRefreshing()
        
        self.refreshData(self.view.refreshControl)
    }
    
    func endRefresh(withUpdates: Bool = false) {
        
        guard self.view.refreshControl != nil else {
            
            return
        }
        
        self.view.refreshControl.endRefreshing()
        
        if withUpdates {
            
            self.view.tableView.reloadRows(at: [IndexPath(row: 0, section: 0), IndexPath(row: 1, section: 0)], with: .fade)
            self.reload()
        }
    }
    
    @objc private func refreshData(_ sender: UIRefreshControl) {
        
        if sender.isRefreshing {
            self.provider.update()
        }
    }
    
    func reload() {
        
        self.view.chart.removeAllSeries()
        
        Services.all.forEach({
            
            let source = $0.source
            if source.getLineVisable() {
                
                self.add(at: source.rawValue)
                self.view.tableView.selectRow(at: IndexPath(row: source.rawValue, section: 0), animated: true, scrollPosition: .none)
            } else {
                
                self.view.tableView.deselectRow(at: IndexPath(row: source.rawValue, section: 0), animated: true)
            }
        })
        
        self.resetLabel()
    }
    
    func checkSelection() {
        
        self.resetLabel()
        
        let count = Services.all.map({
            $0.source
        }).filter({
            $0.getLineVisable()
        }).count
        
        Queues.main.async {
            
            if count != 0 {
            
                self.view.chart.isHidden = false
            }
            
            UIView.animate(withDuration: 0.5, animations: {
                self.view.chart.alpha = count == 0 ? 0.0 : 1.0
            }, completion: { (success) in
                if success {
                    self.view.chart.isHidden = count == 0
                }
            })
        }
    }
    
    func present(error: ErrorTypes) {
    
        let controller = UIAlertController(title: "ERROR", message: error.localizedDescription, preferredStyle: .alert)
        controller.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
        
        self.view.refreshControl.endRefreshing()
        self.view.present(controller)
    }
    
    func resetLabel() {
        
        self.view.valueLabel.attributedText = nil
    }
    
    //MARK: - LineChart
    func add(at index: Int) {
        
        if let service = Services(rawValue: index) {
            
            let result = self.provider.getTickets(for: service.getEndpoint()).sorted(by: {$0.lastUpdate < $1.lastUpdate})
            
            var data = result.compactMap({
                $0.price
            })
            
            if data.count == 1 {
                
                data.insert(result[0].price + 10, at: 0)
            }
            
            let chartSeries = ChartSeries(data)
            chartSeries.color = service.color()
            chartSeries.area = true
            
            self.view.chart.add(chartSeries)
            
            service.set(lineVisable: true)
        }
    }
    
    func remove(at index: Int) {
        
        if let service = Services(rawValue: index) {
        
            service.set(lineVisable: false)
            self.checkSelection()
            
            Queues.main.asyncAfter(deadline: .now() + 0.5) {
                self.reload()
            }
        }
    }
}

extension CCMainPresenter: ChartDelegate {
    
    func didTouchChart(_ chart: Chart, indexes: [Int?], x: Double, left: CGFloat) {
        
        let string = NSMutableAttributedString(string: "")
        
        for (index, dataValue) in indexes.enumerated() {
            
            if let value = chart.valueForSeries(index, atIndex: dataValue) {
                
                let numberFormatter = NumberFormatter()
                numberFormatter.minimumFractionDigits = 2
                numberFormatter.maximumFractionDigits = 2
                
                let color = chart.series[index].color == Services.first.color() ? UIColor.yellow : UIColor.green
                
                var stringValue = numberFormatter.string(from: NSNumber(value: value)) ?? ""
                
                let data = (self.provider.getTickets(for: ((chart.series[index].color == Services.first.color()) ? Services.first : Services.second).getEndpoint()))
                if data.count > 1, let dataValue = data[dataValue!].lastUpdate {
                    
                    let formatter = DateFormatter()
                    formatter.dateFormat = "yyyy-MM-dd HH:mm"
                    stringValue += (" " + formatter.string(from: Date(timeIntervalSince1970: dataValue)))
                }
                
                string.append(NSAttributedString(string: "\n" + stringValue, attributes: [NSAttributedString.Key.foregroundColor : color]))
            }
        }
        
        self.view.valueLabel.attributedText = string
        var constant = self.view.labelLeadingMarginInitialConstant + left - (self.view.valueLabel.frame.width / 2)
        
        if constant < self.view.labelLeadingMarginInitialConstant {
            
            constant = self.view.labelLeadingMarginInitialConstant
        }
        
        let rightMargin = chart.frame.width - self.view.valueLabel.frame.width
        if constant > rightMargin {
            
            constant = rightMargin
        }
        
        self.view.labelLeadingMarginConstraint.constant = constant
    }
    
    func didFinishTouchingChart(_ chart: Chart) {
        
        self.view.valueLabel.attributedText = nil
        self.view.labelLeadingMarginConstraint.constant = self.view.labelLeadingMarginInitialConstant
    }
    
    func didEndTouchingChart(_ chart: Chart) {}
}

extension CCMainPresenter: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return 80
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.add(at: indexPath.row)
        self.checkSelection()
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        
        self.remove(at: indexPath.row)
    }
}
