//
//  CCMainView.swift
//  CryptoCurrency
//
//  Created by Maksim Avksentev on 11/5/18.
//  Copyright © 2018 AvksentevMaks. All rights reserved.
//

import UIKit

protocol CCMainView: class {
    
    var tableView: UITableView! {get set}
    var chart: ChartView! {get set}
    var refreshControl: UIRefreshControl! {get set}
    var valueLabel: UILabel! {get set}
    var labelLeadingMarginConstraint: NSLayoutConstraint! {get set}
    
    var labelLeadingMarginInitialConstant: CGFloat! {get set}
    
    func present(_ controller: UIViewController)
}
